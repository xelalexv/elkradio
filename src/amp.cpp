/*
    Copyright 2019 Alexander Vollschwitz <xelalex@gmx.net>

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
*/

#include <Arduino.h>

#include "amp.h"
#include "buttons.h"

//
Amp::Amp(BluetoothChangeHandler bch) {

    pinMode(PIN_AMP_POWER, OUTPUT);
    setPower(false);
    pinMode(PIN_AMP_VOLUME_UP, OUTPUT);
    digitalWrite(PIN_AMP_VOLUME_UP, LOW);
    pinMode(PIN_AMP_VOLUME_DOWN, OUTPUT);
    digitalWrite(PIN_AMP_VOLUME_DOWN, LOW);

    bluetoothPlaying = false;
    bluetoothFlag = false;
    bluetoothChangeHandler = bch;
}

//
Amp::changeVolume(bool up) {
    int pin = up ? PIN_AMP_VOLUME_UP : PIN_AMP_VOLUME_DOWN;
    digitalWrite(pin, HIGH);
    delay(AMP_VOLUME_DELAY);
    digitalWrite(pin, LOW);
    delay(AMP_VOLUME_DELAY);
}

//
Amp::setPower(bool on) {
    digitalWrite(PIN_AMP_POWER, on ? HIGH : LOW);
}

//
Amp::read() {
    if (bluetoothFlag != readBluetooth()) {
        bluetoothFlag = !bluetoothFlag;
        if (!bluetoothFlag) {
            bluetoothPlaying = !bluetoothPlaying;
            bluetoothChangeHandler(bluetoothPlaying);
        }
    }
}

//
bool Amp::readBluetooth() {
    return !ButtonPanel::readAnalogKey(PIN_BT_PLAY_STATE);
}

//
bool Amp::isBluetoothPlaying() {
    return bluetoothPlaying;
}

//
Amp::toggleBluetoothState() {
    bluetoothPlaying = !bluetoothPlaying;
}

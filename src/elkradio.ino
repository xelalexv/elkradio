/*
    Copyright 2019 Alexander Vollschwitz <xelalex@gmx.net>

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
*/

/*
    refs:
        https://funduino.de/nr-32-der-rotary-encoder-ky-040
        https://github.com/PaulStoffregen/Encoder

        radio:
        http://www.kh-gps.de/fm_china.htm
 */

#include <Arduino.h>

#include "config.h"
#include "state.h"
#include "eeprom.h"
#include "knob.h"
#include "buttons.h"
#include "amp.h"
#include "radio.h"

// ------------------------------------------------------------------ state ---

// persisted state
State* state;

// signals initialization, to block handlers before end of setup is reached
bool initialized;

bool scanByFrequency;

// ------------------------------------------------------------- components ---

PowerVolumeKnob* knob;
ButtonPanel* panel;
Radio* radio;
Amp* amp;

// ----------------------------------------------------------- setup & loop ---

void setup() {

    initialized = false;

    Serial.begin(38400);
    Serial.setTimeout(250);

    DPRINTLN("initializing");

    state = new State();
    loadFromEEPROM(state);
    DPRINT("loaded state: ");
    state->log();

    amp = new Amp(handleBluetoothChange);
    amp->setPower(state->isPowerOn());

    knob = new PowerVolumeKnob(
        handlePowerChange, handleVolumeChange, handleSwitchChange);

    radio = new Radio();

    panel = new ButtonPanel();

    // single keys
    panel->addHandler(new KeyEventHandler(KEY_MASK_PREV, "PREV", h_KEY_PREV));
    panel->addHandler(new KeyEventHandler(KEY_MASK_NEXT, "NEXT", h_KEY_NEXT));
    panel->addHandler(new KeyEventHandler(KEY_MASK_BND, "BND", h_KEY_BND));
    panel->addHandler(new KeyEventHandler(KEY_MASK_1, "1", h_KEY_1));
    panel->addHandler(new KeyEventHandler(KEY_MASK_2, "2", h_KEY_2));
    panel->addHandler(new KeyEventHandler(KEY_MASK_3, "3", h_KEY_3));
    panel->addHandler(new KeyEventHandler(KEY_MASK_4, "4", h_KEY_4));
    panel->addHandler(new KeyEventHandler(KEY_MASK_5, "5", h_KEY_5));
    panel->addHandler(new KeyEventHandler(KEY_MASK_AVI, "AVI", h_KEY_AVI));

    // composite keys
    panel->addHandler(new KeyEventHandler(
        KEY_MASK_BND | KEY_MASK_PREV, "BND+PREV", h_KEY_BND_PREV));
    panel->addHandler(new KeyEventHandler(
        KEY_MASK_BND | KEY_MASK_NEXT, "BND+NEXT", h_KEY_BND_NEXT));

    // ---------------------------------------- unused pins ---
    pinMode(5, INPUT_PULLUP);

    // --------------------------------------- set up radio ---
    delay(3000);
    radio->setDefaultVolume(state->getRadioVolume());
    radio->setMuted(state->isRadioMuted());
    radio->setCampus(false);
    radio->light(!state->isRadioMuted());
    scanByFrequency = false;

    initialized = true;

    logRadioStatus();
}

void loop() {

    amp->read();
    knob->read();
    panel->read();

    // eat up any left over replies from radio module
    while (Serial.available() > 0) {
        Serial.read();
    }
}

// ----------------------------------------------------------- power switch ---

void handleSwitchChange(KeyEvent e) {
    if (isReleased(e) && !isBluetoothPlaying()) {
        bool mute = state->toggleRadioMute();
        DPRINTLN(mute ? "radio mute" : "radio unmute");
        radio->light(!mute);
        radio->setMuted(mute);
        storeToEEPROM(state);
    }
}

void handlePowerChange() {
    if (initialized) {
        bool power = state->togglePower();
        DPRINTLN(power ? "power on" : "power off");
        amp->setPower(power);
        storeToEEPROM(state);
    }
}

// ------------------------------------------------------------ volume knob ---

void handleVolumeChange(bool up) {
    if (!initialized) return;
    amp->changeVolume(up);
}

// --------------------------------------------------- Bluetooth play state ---

void handleBluetoothChange(bool playing) {
    if (!initialized) return;
    if (playing) {
        DPRINTLN("Bluetooth started playing");
    } else {
        DPRINTLN("Bluetooth stopped playing");
    }
    radio->light(!playing && !state->isRadioMuted());
}

// ----------------------------------------------------- key event handlers ---

bool h_KEY_AVI(KeyEvent e) {
    if (isReleased(e)) {
        radio->light(true);
        radio->stopScan();
        return true;
    }
    if (isLongPress(e) && isRadioPlaying()) {
        radio->light(true);
        radio->scan();
        return true;
    }
    return false;
}

bool h_KEY_PREV(KeyEvent e) {
    return handleRadioScan(false, e);
}

bool h_KEY_NEXT(KeyEvent e) {
    if (isLongPress(e) && isRadioPlaying()) {
        radio->light(true);
        String ret = radio->setCampus(true);
        DPRINT("campus: ");
        DPRINTLN(ret);
        return true;
    }
    return handleRadioScan(true, e);
}

bool h_KEY_BND(KeyEvent e) {
    if (isLongPress(e)) {
        // escape hatch - sometimes Bluetooth state is flipped, and there's no
        // definite way to determine whether Bluetooth is playing or not; if
        // it's the wrong way, user can correct with long press on BND
        radio->light(true);
        amp->toggleBluetoothState();
        DPRINT("toggled amp Bluetooth state to ");
        DPRINTLN(!isBluetoothPlaying());
        return true;
    }
    if (isReleased(e) && isRadioPlaying()) {
        radio->light(true);
        scanByFrequency = !scanByFrequency;
        DPRINTLN(
            scanByFrequency ? "scan by frequency" : "scan by station");
        return true;
    }
    return false;
}

bool h_KEY_BND_PREV(KeyEvent e) {
    return handleRadioVolume(false, e);
}

bool h_KEY_BND_NEXT(KeyEvent e) {
    return handleRadioVolume(true, e);
}

bool h_KEY_1(KeyEvent e) {
    return handleStationKey(e, 0);
}

bool h_KEY_2(KeyEvent e) {
    return handleStationKey(e, 1);
}

bool h_KEY_3(KeyEvent e) {
    return handleStationKey(e, 2);
}

bool h_KEY_4(KeyEvent e) {
    return handleStationKey(e, 3);
}

bool h_KEY_5(KeyEvent e) {
    return handleStationKey(e, 4);
}

// -------------------------------------------------- event handler helpers ---

bool handleRadioScan(bool up, KeyEvent e) {
    if (isRadioPlaying()) {
        if (isReleased(e)) {
            radio->light(true);
            if (scanByFrequency) {
                up ? radio->scanUp() : radio->scanDown();
            } else {
                up ? radio->channelUp() : radio->channelDown();
            }
            return true;
        }
        if (isLongPress(e)) {
            radio->light(true);
            scanByFrequency = !scanByFrequency;
            DPRINTLN(scanByFrequency ? "scan by frequency" : "scan by station");
            return true;
        }
    }
    return false;
}

bool handleRadioVolume(bool up, KeyEvent e) {
    if (isPressed(e) && isRadioPlaying()) {
        radio->light(true);
        String vol = up ? radio->volumeUp() : radio->volumeDown();
        DPRINT("radio volume now: ");
        DPRINTLN(vol);
        state->setRadioVolume(vol);
        radio->setDefaultVolume(vol);
        storeToEEPROM(state);
        return true;
    }
    return false;
}

bool handleStationKey(KeyEvent event, int slot) {

    if (!initialized || !isRadioPlaying()) {
        return false;
    }

    Station* st = state->getStation(slot);

    if (st != NULL) {

        switch (event) {
            case KEY_PRESSED:
                radio->light(false);
                break;
            case KEY_RELEASED:
                DPRINT("tuning radio to station ");
                DPRINTLN(slot);
                radio->light(true);
                radio->setFrequency(st->getFrequency());
                return true;
            case KEY_LONG_PRESS:
                DPRINT("storing station ");
                DPRINTLN(slot);
                radio->light(true);
                st->setFrequency(radio->getFrequency());
                storeToEEPROM(state);
                return true;
        }
    }

    return false;
}

bool isPressed(KeyEvent e) {
    return initialized && (e == KEY_PRESSED);
}

bool isReleased(KeyEvent e) {
    return initialized && (e == KEY_RELEASED);
}

bool isLongPress(KeyEvent e) {
    return initialized && (e == KEY_LONG_PRESS);
}

bool isBluetoothPlaying() {
    return amp->isBluetoothPlaying();
}

bool isRadioPlaying() {
    return !isBluetoothPlaying() && !state->isRadioMuted();
}

void logRadioStatus() {
    DPRINTLN("radio status:");
    DPRINT("frequency: ");
    DPRINTLN(radio->getFrequency());
    DPRINT("channel: ");
    DPRINTLN(radio->getChannel());
    DPRINT("volume: ");
    DPRINTLN(radio->getVolume());
}

// -------------------------------------------------------------------- LED ---

void flashLED(int count) {
    for (int i = 0; i < count; i++) {
        ledOn();
        delay(250);
        ledOff();
        delay(250);
    }
}

void ledOn() {
    PORTB = 0x3f;
}

void ledOff() {
    PORTB = 0x1f;
}

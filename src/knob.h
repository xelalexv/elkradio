/*
    Copyright 2019 Alexander Vollschwitz <xelalex@gmx.net>

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
*/

#ifndef knob_h
#define knob_h

#include "_Encoder.h"
#include "buttons.h"

// power switch
const int PIN_POWER_SWITCH = 2;

const uint8_t PIN_MASK_POWER_SWITCH = 0x04;

const uint32_t MIN_POWER_TOGGLE_MILLIS   = 500;

// volume knob
const int PIN_VOLUME_CLK = 3;
const int PIN_VOLUME_DT  = 4;

const int CLICK_STEPS = 4;
const int CLICK_COUNT = 1;

//
typedef void (*SwitchHandler) (KeyEvent);
typedef void (*PowerChangeHandler) ();
typedef void (*VolumeChangeHandler) (bool);

//
class PowerVolumeKnob {

private:
	Encoder* knob;

	uint32_t powerSwitchEventTime;
	bool powerSwitchPressed;

	long volIntermittent;
	long volume;

	SwitchHandler switchHandler;
	PowerChangeHandler powerChangeHandler;
	VolumeChangeHandler volumeChangeHandler;

	readSwitch();
	readVolume();

public:
	PowerVolumeKnob(PowerChangeHandler pch, VolumeChangeHandler vch,
		SwitchHandler sh);
	read();
};

#endif

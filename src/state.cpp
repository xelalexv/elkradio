/*
    Copyright 2019 Alexander Vollschwitz <xelalex@gmx.net>

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
*/

#include "state.h"
#include "config.h"

// ---------------------------------------------------------------- Station ---

//
Station::Station() {
    reset();
}

//
Station::reset() {
    setFrequency("875");

}

//
String Station::getFrequency() {
    return String(frequency);
}

//
Station::setFrequency(String f) {
    f.toCharArray(frequency, array_len(frequency));
}

//
Station::log() {
    DPRINT(frequency);
}

// ------------------------------------------------------------------ State ---

//
State::State() {
    reset();
}

//
State::reset() {
    power = false;
    radioMuted = true;
    setRadioVolume("10");
    writes = 0;
    for (int ix = 0; ix < array_len(stations); ix++) {
        stations[ix].reset();
    }
}

//
bool State::isPowerOn() {
    return power;
}

//
bool State::togglePower() {
    power = !power;
    return power;
}

//
State::setPower(bool on) {
    power = on;
}

//
bool State::isRadioMuted() {
    return radioMuted;
}

//
bool State::toggleRadioMute() {
    radioMuted = !radioMuted;
    return radioMuted;
}

//
State::setRadioMuted(bool muted) {
    radioMuted = muted;
}

//
String State::getRadioVolume() {
    return String(radioVolume);
}

//
State::setRadioVolume(String v) {
    v.toCharArray(radioVolume, array_len(radioVolume));
}

//
Station* State::getStation(int ix) {
    if (0 <= ix && ix < array_len(stations)) {
        return &stations[ix];
    }
    return NULL;
}

//
State::log() {

#if DEBUG == true

    DPRINT("{ power: ");
    DPRINT(power ? "on" : "off");
    DPRINT(", radio: ");
    DPRINT(radioMuted ? "muted/" : "unmuted/");
    DPRINT(radioVolume);
    DPRINT(", [");
    for (int ix = 0; ix < array_len(stations); ix++) {
        if (ix > 0) {
            DPRINT(", ");
        }
        stations[ix].log();
    }
    DPRINT("], ");
    DPRINT(String(writes));
    DPRINTLN(" writes }");

#endif
}

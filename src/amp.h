/*
    Copyright 2019 Alexander Vollschwitz <xelalex@gmx.net>

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
*/

#ifndef amp_h
#define amp_h

// amp states & keys - these are taken from the amp module, which uses 1.8V TTL
// (low=0V, high=1.8V); we therefore use analog inputs
const int PIN_AMP_VOLUME_UP   = A1;
const int PIN_AMP_VOLUME_DOWN = A2;
const int PIN_AMP_POWER       = A4;
const int PIN_BT_PLAY_STATE   = A5;

const uint32_t AMP_VOLUME_DELAY = 70;

//
typedef void (*BluetoothChangeHandler) (bool);

//
class Amp {

private:
	bool bluetoothPlaying;
	bool bluetoothFlag;
	BluetoothChangeHandler bluetoothChangeHandler;

	bool readBluetooth();

public:
	Amp(BluetoothChangeHandler bch);
	changeVolume(bool up);
	setPower(bool on);
	read();

	bool isBluetoothPlaying();
	toggleBluetoothState();
};

#endif

/*
    Copyright 2019 Alexander Vollschwitz <xelalex@gmx.net>

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
*/

#include "radio.h"

//
Radio::Radio() {
}

//
String Radio::sendCommand(String cmd, bool getReply) {
	Serial.flush();
	delay(100);
    Serial.println(cmd);
	Serial.flush();
    return getReply ? receiveReply() : "";
}

//
String Radio::receiveReply() {
    return Serial.readString();
}

//
String Radio::status() {
    return sendCommand("AT+RET", true);
}

//
//String Radio::status() {
//    return "/***********************************/\nVOL=00\nFRE=985\nCH=16\n******************************/";
//}

//
Radio::setMuted(bool m) {
    if (m) {
    	mute();
	} else {
		unmute();
	}
}

//
Radio::light(bool on) {
    sendCommand(on ? "AT+BANK=10" : "AT+BANK=00", true);
}

//
String Radio::pause() {
    return sendCommand("AT+PAUS", true);
}

//
Radio::mute() {
    sendCommand("AT+VOL=00", false);
}

//
Radio::unmute() {
    sendCommand("AT+VOL=" + defaultVolume, false);
}

//
String Radio::volumeUp() {
    return extractValue(sendCommand("AT+VOLU", true), "VOL=", 2, 2);
}

//
String Radio::volumeDown() {
    return extractValue(sendCommand("AT+VOLD", true), "VOL=", 2, 2);
}

//
String Radio::getVolume() {
    return extractValue(status(), "VOL=", 2, 2);
}

//
Radio::setDefaultVolume(String v) {
    defaultVolume = v;
}

//
String Radio::setCampus(bool c) {
    return sendCommand(c ? "AT+CAMPUS=1" : "AT+CAMPUS=0", true);
}

//
Radio::scan() {
    sendCommand("AT+SCAN", false);
}

// FIXME: not recognized by FM module, instead, auto scan is started
Radio::stopScan() {
//    sendCommand("AT+SCANSTOP", false);
}

//
Radio::scanUp() {
    sendCommand("AT+SCANU", false);
}

//
Radio::scanDown() {
    sendCommand("AT+SCAND", false);
}

//
String Radio::channelUp() {
    return sendCommand("AT+CHD", false);
}

//
String Radio::channelDown() {
    return sendCommand("AT+CHU", false);
}

//
String Radio::getFrequency() {
    return extractValue(status(), "FRE=", 3, 4);
}

//
Radio::setFrequency(String f) {
    sendCommand("AT+FRE=" + f, false);
}

//
String Radio::getChannel() {
    return extractValue(status(), "CH=", 1, 3);
}

//
String Radio::extractValue(String data, String token, int min, int max) {

    int start = data.indexOf(token);
    if (start == -1) {
        return "";
    }

    start += token.length();
    int end = data.indexOf(0x0d, start);
    if (end == -1) {
        end = data.length();
    }

    if (end - start < min) {
        return "";
    }
    end = (end - start) > max ? start + max : end;

    return data.substring(start, end);
}

/*
    Copyright 2019 Alexander Vollschwitz <xelalex@gmx.net>

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
*/

#ifndef eeprom_h
#define eeprom_h

#include <EEPROM.h>

#include "_CRC32.h"

// start of EEPROM is used for index
const int EEPROM_START = sizeof(int);

//
int validateIndex(int ix) {
    if (ix < EEPROM_START) {
        ix = EEPROM_START;
    } else {
        int last = ix + sizeof(uint32_t) + sizeof(State);
        if (last > E2END) {
            return EEPROM_START;
        }
    }
    return ix;
}

//
int seekEEPROM(bool advance) {

    int ixR;
    EEPROM.get(0, ixR);

    DPRINT("EEPROM index read is ");
    DPRINTLN(ixR);

    int ixV = validateIndex(ixR);

    DPRINT("EEPROM index validated is ");
    DPRINTLN(ixV);

    if (ixR != ixV) {
        EEPROM.put(0, ixV);
	    DPRINT("EEPROM index corrected to ");
	    DPRINTLN(ixV);

    }
    if (advance) {
	    ixV = validateIndex(++ixV);
        EEPROM.put(0, ixV);
	    DPRINT("EEPROM index advanced is ");
	    DPRINTLN(ixV);
    }

    DPRINT("EEPROM index used is ");
    DPRINTLN(ixV);

    return ixV;
}

//
uint32_t crc(State *s) {
    CRC32 crc;
    crc.update(*s);
    return crc.finalize();
}

//
void storeToEEPROM(State* s) {

    s->writes++;

    int ix;
    if (s->writes > 100) {
    	ix = seekEEPROM(true);
    	s->writes = 0;
    } else {
    	ix = seekEEPROM(false);
    }

    DPRINT("storing state to index ");
    DPRINTLN(ix);

    uint32_t sum = crc(s);
    EEPROM.put(ix, sum);
    EEPROM.put(ix + sizeof(sum), *s);
}

//
void loadFromEEPROM(State* s) {

    int ix = seekEEPROM(false);

    DPRINT("loading state from index ");
    DPRINTLN(ix);

    uint32_t sum;
    EEPROM.get(ix, sum);
    EEPROM.get(ix + sizeof(sum), *s);

    if (sum == crc(s)) {
        DPRINTLN("state CRC OK");
    } else {
        DPRINTLN("corrupted state, resetting");
        s->reset();
    }
}

#endif

/*
    Copyright 2019 Alexander Vollschwitz <xelalex@gmx.net>

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
*/

#ifndef buttons_h
#define buttons_h

// pin assignments for keys
const int PIN_KEY_BND  = 6;
const int PIN_KEY_AVI  = 7;
const int PIN_KEY_1    = 8;
const int PIN_KEY_2    = 9;
const int PIN_KEY_3    = 10;
const int PIN_KEY_4    = 11;
const int PIN_KEY_5    = 12;
const int PIN_KEY_NEXT = A6;
const int PIN_KEY_PREV = A7;

// key masks for use with `ButtonPanel::keys`
const uint16_t KEY_MASK_BND  = 0x0040;
const uint16_t KEY_MASK_AVI  = 0x0080;
const uint16_t KEY_MASK_1    = 0x0001;
const uint16_t KEY_MASK_2    = 0x0002;
const uint16_t KEY_MASK_3    = 0x0004;
const uint16_t KEY_MASK_4    = 0x0008;
const uint16_t KEY_MASK_5    = 0x0010;
const uint16_t KEY_MASK_PREV = 0x0100;
const uint16_t KEY_MASK_NEXT = 0x0200;

const int MAX_KEY_EVENT_HANDLERS = 16;

const uint32_t LONG_PRESS_LIMIT = 3000;

// ------------------------------------------------------ key event handler ---

enum KeyEvent {
	KEY_PRESSED,
	KEY_RELEASED,
	KEY_LONG_PRESS
};

typedef bool (*HandlerFunc) (KeyEvent);

class KeyEventHandler {

private:
	char* label;
	uint16_t mask;
	HandlerFunc handler;

public:
	KeyEventHandler(uint16_t mask, char* label, HandlerFunc h);
	bool checkEvent(uint16_t keys, uint16_t previous, bool longPress);
};


// ----------------------------------------------------------- button panel ---

class ButtonPanel {

private:
	uint16_t keys;
	uint32_t lastKeyEventTime;
	bool sequenceConsumed;

	KeyEventHandler* keyEventHandlers[MAX_KEY_EVENT_HANDLERS];
	int nextEventHandlerSlot;

	uint16_t readKeys();
	bool forwardEvent(int keys, int previous, bool longPress);

public:
	ButtonPanel();
	addHandler(KeyEventHandler* keh);
	read();

	static bool readAnalogKey(int pin);
};

#endif

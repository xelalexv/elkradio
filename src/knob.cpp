/*
    Copyright 2019 Alexander Vollschwitz <xelalex@gmx.net>

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
*/

#include <Arduino.h>

#include "knob.h"
#include "config.h"

//
PowerVolumeKnob::PowerVolumeKnob(
    PowerChangeHandler pch, VolumeChangeHandler vch, SwitchHandler sh) {

    powerSwitchPressed = false;
    powerSwitchEventTime = 0;
    powerChangeHandler = pch;
    switchHandler = sh;

	volume = 0;
	volIntermittent = 0;
	volumeChangeHandler = vch;

    pinMode(PIN_POWER_SWITCH, INPUT_PULLUP);
	knob = new Encoder(PIN_VOLUME_CLK, PIN_VOLUME_DT);
}

//
PowerVolumeKnob::read() {
    readSwitch();
    readVolume();
}

//
PowerVolumeKnob::readVolume() {

    long v = knob->read();
    if (v == volIntermittent) {
    	return;
	}

	volIntermittent = v;

    if (abs(volume - volIntermittent) >= CLICK_COUNT * CLICK_STEPS) {

        bool up = volIntermittent > volume;
        volume = volIntermittent;

        DPRINT("vol: ");
        DPRINT(volume);
        DPRINT(up ? ", up" : ", down");
        DPRINTLN();

        volumeChangeHandler(up);
    }
}

//
PowerVolumeKnob::readSwitch() {

    // H on power switch pin means open switch
    bool pressed = !digitalRead(PIN_POWER_SWITCH);

    if (pressed == powerSwitchPressed) {
        if (powerSwitchEventTime > 0 &&
            millis() - powerSwitchEventTime > MIN_POWER_TOGGLE_MILLIS) {
            powerSwitchEventTime = 0;
            if (pressed) {
                DPRINTLN("power state toggled");
                powerChangeHandler();
            }
        }
        return;
    }

    powerSwitchPressed = pressed;
    powerSwitchEventTime = millis(); // new change, start clock

    if (pressed) {
        DPRINTLN("power switch pressed");
        switchHandler(KEY_PRESSED);
    } else {
        DPRINTLN("power switch released");
        switchHandler(KEY_RELEASED);
    }
}

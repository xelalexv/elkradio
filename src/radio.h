/*
    Copyright 2019 Alexander Vollschwitz <xelalex@gmx.net>

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
*/

#ifndef radio_h
#define radio_h

#include <Arduino.h>

//
class Radio {

private:
	String defaultVolume;

	String sendCommand(String cmd, bool getReply);
	String receiveReply();
	String extractValue(String data, String token, int min, int max);

public:
	Radio();
	String status();
	light(bool on);

	String pause();
	mute();
	unmute();
	setMuted(bool m);
	String volumeUp();
	String volumeDown();
	String getVolume();
	setDefaultVolume(String vol);

	String setCampus(bool c);
	scan();
	stopScan();
	scanUp();
	scanDown();
	String channelUp();
	String channelDown();
	setFrequency(String f);
	String getFrequency();
	String getChannel();
};

#endif

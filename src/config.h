/*
    Copyright 2019 Alexander Vollschwitz <xelalex@gmx.net>

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
*/

#ifndef elkradio_h
#define elkradio_h

// Set whether to activate debug mode. In debug mode, diagnostic messages are
// sent over the serial port. This may confuse the radio module though, since
// it is using the same serial port.
#define DEBUG false


// --- debug helpers ---------------------------------------------------------

#if DEBUG == true

#include <Arduino.h>

#define DPRINT(...)    Serial.print(__VA_ARGS__)
#define DPRINTLN(...)  Serial.println(__VA_ARGS__); Serial.flush()

#else

#define DPRINT(...)
#define DPRINTLN(...)

#endif


// --- other helpers ---------------------------------------------------------

#define array_len( x )  ( sizeof( x ) / sizeof( *x ) )


// ---------------------------------------------------------------------------

#endif

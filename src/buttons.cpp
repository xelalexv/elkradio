/*
    Copyright 2019 Alexander Vollschwitz <xelalex@gmx.net>

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
*/

#include <Arduino.h>

#include "buttons.h"
#include "config.h"

// ------------------------------------------------------ key event handler ---

KeyEventHandler::KeyEventHandler(uint16_t mask, char* label, HandlerFunc h) {
	this->mask = mask;
	this->label = label;
	this->handler = h;
}

bool KeyEventHandler::checkEvent(uint16_t keys, uint16_t previous,
    bool longPress) {

    if (longPress && (keys & mask) == mask) { // handle long press
        return handler(KEY_LONG_PRESS);
    }

    if (((previous ^ keys) & mask) != 0) { // handle key state change
        if ((keys & mask) == mask) {
            DPRINT("key pressed: ");
            DPRINTLN(label);
            return handler(KEY_PRESSED);
        } else if ((keys & mask) == 0) {
            DPRINT("key released: ");
            DPRINTLN(label);
            return handler(KEY_RELEASED);
        }
    }

    return false; // nothing for this handler
}

// ----------------------------------------------------------- button panel ---

ButtonPanel::ButtonPanel() {

    pinMode(PIN_KEY_BND, INPUT_PULLUP);
    pinMode(PIN_KEY_AVI, INPUT_PULLUP);
    pinMode(PIN_KEY_1, INPUT_PULLUP);
    pinMode(PIN_KEY_2, INPUT_PULLUP);
    pinMode(PIN_KEY_3, INPUT_PULLUP);
    pinMode(PIN_KEY_4, INPUT_PULLUP);
    pinMode(PIN_KEY_5, INPUT_PULLUP);

    nextEventHandlerSlot = 0;
    sequenceConsumed = false;
    lastKeyEventTime = 0;
	keys = 0;

    for (int ix = 0; ix < MAX_KEY_EVENT_HANDLERS; ix++) {
        keyEventHandlers[ix] = NULL;
    }
}

/*
    Add a key event handler to this button panel. Note that the order in which
    handlers are added matters, since a handler may declare a key stroke
    sequence consumed, thus stopping further propagation of events for that
    sequence.
 */
ButtonPanel::addHandler(KeyEventHandler* keh) {
	if ((keh != NULL) && nextEventHandlerSlot < MAX_KEY_EVENT_HANDLERS) {
		keyEventHandlers[nextEventHandlerSlot] = keh;
		nextEventHandlerSlot++;
	}
}

bool ButtonPanel::forwardEvent(int keys, int previous, bool longPress) {
    for (int ix = 0; ix < MAX_KEY_EVENT_HANDLERS; ix++) {
        if (keyEventHandlers[ix] != NULL) {
            if (keyEventHandlers[ix]->checkEvent(keys, previous, longPress)) {
                return true;
            }
        }
    }
    return false;
}

/*
    Read handles key stroke sequences. A sequence is a series of key presses &
    releases, starting and ending with no keys pressed, and always having at
    least one key pressed. Changes within the sequence are signaled to all
    key event handlers until one of the handlers declares the sequence as
    consumed, or the sequence ends. A long press event is signaled if there is
    no key state change within a sequence for a certain period of time, and the
    sequence has not been consumed yet.
 */
ButtonPanel::read() {

    uint16_t k = readKeys();

    if (k == keys) { // no changes
        if (sequenceConsumed || k == 0) {
            return; // sequence consumed or no keys pressed
        }

        // check for long press
        uint32_t now = millis();
        if (0 < lastKeyEventTime &&
            (now - lastKeyEventTime) > LONG_PRESS_LIMIT) {
            lastKeyEventTime = 0;
            DPRINTLN("long press detected");
            sequenceConsumed = forwardEvent(k, keys, true);
        }

        return;
    }

    lastKeyEventTime = millis();

    if (!sequenceConsumed) {
        sequenceConsumed = forwardEvent(k, keys, false);
    }

    if (k == 0) { // change to all keys open, i.e. end of sequence
        sequenceConsumed = false;
    }
    keys = k;
}

/*
    PORTB (bits 0 through 5) has the 5 number keys assigned, PORTD BND key
    (bit 6) and AVI key (bit 7), with 0 representing closed switch. The PREVIOUS
    and NEXT keys are assigned to analog inputs, since they are shared with the
    amp module (0V is open switch, 1.8V closed switch). All of this is read into
    the return value as below, with 1 representing closed switch:

    bit      9      8      7      6      5      4      3      2      1      0
    key     next  prev    AVI    BND     0     key5   key4   key3   key2   key1
 */
uint16_t ButtonPanel::readKeys() {
    uint16_t ret = (~((PINB & 0x1f) | (PIND & 0xc0))) & 0xdf;
    ret |= readAnalogKey(PIN_KEY_PREV) ? KEY_MASK_PREV : 0x0;
    ret |= readAnalogKey(PIN_KEY_NEXT) ? KEY_MASK_NEXT : 0x0;
    return ret;
}

static bool ButtonPanel::readAnalogKey(int pin) {
    return analogRead(pin) > 200;
}

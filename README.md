
# elkradio

The *elkradio* is an old *Volvo CR-606* car stereo that I rebuilt to go in my classic 1988 *Volvo 480ES*. I wanted something original *Volvo* from the 1980ies, but at the same time did also want the convenience of Bluetooth replay from my phone. I did not want to use a cassette tape adapter, because the experience I had with those in the past was not the best. In addition, I really wanted to be able to control Bluetooth playback with the buttons on the car stereo's front panel.

So I essentially emptied out the device and rebuilt it using a cheap 2 x 50 Watt Bluetooth amp and an FM receiver module. What I kept form the original PCB were the sections with the buttons and the rear connector, which also had a fuse and noise filter on the supply voltage line. I added an *Arduino Nano* to stitch everything together. That's about it.

![Bluetooth amp](doc/bt-amp.jpg)

*The Bluetooth amp*

![Bluetooth amp](doc/radio.jpg)

*The FM receiver module*

## Is this a construction manual?
**No!** I don't think this will be of much use to anyone as is, since you would have to use the exact same components that I used. Mostly this serves as documentation for myself, should I have to do some work on this again in the future. It could still be an inspiration for similar projects, and it shouldn't be too hard to adapt the code to a different setup. You will however need enough knowledge about electronics and electrical circuits to build something like this yourself!

## Button Assignments

This is the front panel of the *Volvo CR-603*. I have the *CR-606*, but the only difference is that station key *6* is labeled *AVI*.

![front panel](doc/cr-603-front-panel.jpg)

1. **Power button & volume** - A short press mutes/unmutes the radio, a long press toggles the power state of the whole unit.
2. **Pause/Play** - This controls replay of the connected Bluetooth device.
3. **Scan Mode** - When radio is on, toggles the mode for the `<` and `>` buttons between switching to previous/next channel (default after power on) and scanning down/up. Channels need to be set up with a full scan (*AVI*).
4. **Previous/Next** - When playing in Bluetooth mode, this skips to previous/next track. When radio is playing, this either switches to previous/next channel, or scans down/up, depending on mode (3).
5. The radio display is now in the tape cassette slot. There is a 3.5 mm line-in jack.
6. **Station Buttons/Auto-Scan** - A short press on any of the station buttons tunes to the frequency stored on the pressed button, a long press saves currently tuned in frequency to it. A long press on the *AVI* button starts an auto-scan, a short press lights up the radio display.
7. no function
8. Radio display.
9. No function, removable, hides the USB port for programming the *Arduino Nano*.

## Putting It All Together

![front panel](doc/elkradio.jpg)

*A bit of a mess, with the point-to-point wiring, but good enough for a prototype. Just didn't want to bother with creating a PCB.*

- 12V power supply for the unit is taken only from the *ACC* pin (terminal 15) on the harness connector. The *BAT* pin (terminal 30) is not used. All settings are stored to EEPROM on the *Arduino*, so they persist even if the unit is completely disconnected. Writing to EEPROM is leveled, i.e. write location is slowly rotating through available EEPROM.
- Power for the amp & radio is switched via a relay, which is controlled by a digital output + transistor. The *Arduino* is always on, as long as *ACC* is switched on.
- The original power/volume knob is replaced with a *KY-040* style rotary encoder/switch, which uses 3 digital pins on the *Arduino*.
- The `>|`, `<`, and `>` buttons on the front panel are directly wired to the amp unit, parallel to its corresponding buttons. The *Arduino* ties in into the `<` and `>` buttons via two analogue inputs, because the particular amp unit I have uses 1.8V logic for its buttons. Two digital outputs are connected via voltage dividers to the volume up/down buttons for changing amp volume.
- All other buttons from the front panel are directly connected to digital inputs on the *Arduino*.
- The FM receiver module has a serial port, which uses 3.3V logic. Its `Tx` is directly connected to the *Arduino's* `Rx`, and the `Rx` via a voltage divider to the *Arduino's* `Tx`. I additionally ran the two serial port lines through a *CD4016* (quad bilateral switch), to disconnect them when the supply voltage of the module is off, because the *Arduino* is still running and the voltage from its `Tx` line might damage the FM module. Maybe that's not really necessary, just didn't want to take any chances. Probably a simple level shift module would have been better, just didn't have one around, but lots of the *CD4016*.

## Third Party Libraries

- [CRC32 checksum calculation](https://github.com/bakercp/CRC32) by Christopher Baker
- [encoder library](https://github.com/PaulStoffregen/Encoder) by Paul Stoffregen
